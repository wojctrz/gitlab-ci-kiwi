import unittest
import random

class ExampleTest(unittest.TestCase):

    def test_that_will_pass(self):
        self.assertEqual(1+1, 2)

    def test_that_will_fail(self):
        self.assertTrue(False)

    def test_that_can_fail(self):
        radom = random.randint(0, 10)
        self.assertLessEqual(radom, 5)

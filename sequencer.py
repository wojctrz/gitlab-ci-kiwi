import unittest
import xmlrunner

from example_test import ExampleTest

loader = unittest.TestLoader()
tests = list()
tests.append(loader.loadTestsFromTestCase(ExampleTest))
suite = unittest.TestSuite(tests)

with open(f"test_report.xml", 'wb') as output:
    runner = xmlrunner.XMLTestRunner(output=output)
    result = runner.run(suite)
